
//جی کوئری مربوط به owl
$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        items: 1,
        dots: false,
        loop: true,
        center: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        animateOut: true,
        animateIn: true,

    });
});


// جدا سازی ارقام 
var price = document.getElementsByClassName("price");
for (i = 0; i < price.length; i++) {
    // number=new Intl.NumberFormat('fa-IR', { style: 'currency', currency: 'IRR' }).format(price[i].innerHTML);
    number = new Intl.NumberFormat('fa-IR').format(price[i].innerHTML);
    document.getElementsByClassName("price")[i].innerHTML = number;
    // console.log(number)
}

//باز و بسته شدن توضیحات
function toggleDes(id, td) {
    var ddelement = document.getElementById(id);
    var tdelement = document.getElementById(td);

    // ddelement.style.display = ddelement.style.display !== 'block' ? 'block' : 'none';
    if (ddelement.style.display !== 'block') {
        ddelement.style.display = 'block';
        ddelement.className = 'dd-show'
        tdelement.className = 'dt-show';
    } else {
        ddelement.style.display = 'none';
        tdelement.className = '';

    }
    // console.log("clicked");
}

// نمایش سه خط از پاراگراف
function more(textid, btnid) {
    var textlimited = document.getElementById(textid);
    var moreBtn = document.getElementById(btnid);
    // console.log(moreBtn);
    if (textlimited.style.display !== 'block') {
        textlimited.style.display = 'block';
        moreBtn.textContent = 'کمتر';
    } else {
        textlimited.style.display = '-webkit-box';
        moreBtn.textContent = 'بیشتر';
    }
}
//نمایش دکمه هاس اشتراک گذاری
function share(buttons) {
    var sociallink = document.getElementById(buttons);
    if (sociallink.style.visibility !== 'visible') {
        sociallink.style.visibility = 'visible';
    } else {
        sociallink.style.visibility = 'hidden';
    }
}

// منو همبرگری
function openMenu() {
    var menu = document.getElementById('menu');
    var nav = document.getElementById('navtag');
    var navlogo = document.getElementById('logo');
    var hamburgermenuicon = document.getElementsByClassName('hamburger-menu')[0];

    if (menu.style.display !== 'block') {
        menu.style.display = 'block';
        navlogo.style.display = 'none';
        hamburgermenuicon.src = './images/close.svg';
        nav.classList.add("re-col");
        hamburgermenuicon.classList.add("mt");
    } else {
        menu.style.display = 'none';
        navlogo.style.display = 'block';
        hamburgermenuicon.src = './images/hamburger-menu.svg';
        nav.classList.remove("re-col");
        hamburgermenuicon.classList.remove("mt");

    }
}

