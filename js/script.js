// تاریخ شمسی
jalaliDatepicker.startWatch();


var flyclass = document.getElementById("fly-class");
var childcount = document.getElementById("child-count");
var passengercount = document.getElementById("passengercount");
var source = document.getElementById("source");
var target = document.getElementById("target");

var fly = document.getElementById("fly");
var hotel = document.getElementById("hotel");
var flyhotel = document.getElementById("flyhotel");
var tour = document.getElementById("tour");
var insurance = document.getElementById("insurance");
var hamburgermenu = document.getElementById("hamburgermenu");
var menu = document.getElementById("menu-hamburger");
var linkhamburger = document.getElementById("link-list-hamburger");
var body = document.getElementsByTagName("body")[0];

//در زمان تغییر سایز پنجره درصورتی که از حدی بیشتر شود که منو کامل نمایش داده شود منو را کامل نمایش دهد 
// window.addEventListener('resize', function(event) {
//     if (window.innerWidth > 535 ) {
//         menu.style.display = "block";
//     }else{
//         menu.style.display = "none";
//         hamburgermenu.src = './images/hamburger-menu-svgrepo-com (1).svg';
//     }
// }, true);

// باز و بسته شدن منو همبرگری 
function openMenu() {
    if (menu.style.display !== 'block') {
        menu.style.display = 'block';
        hamburgermenu.src = './images/icons8-close-30.png';
        body.classList.add('overlay');
    } else if (menu.style.display == 'block' && window.innerWidth < 535) {
        menu.style.display = 'none';
        body.classList.remove('overlay');
        hamburgermenu.src = './images/hamburger-menu-svgrepo-com (1).svg';
    }

}



// بسته شدن منو در صورت کلیک در خارج از زیر منو
window.addEventListener('click', function (e) {
    if (window.innerWidth < 535 && e.target != document.querySelector(".hamburgermenu-icon")
        && e.target !== document.querySelector(".link-list-hamburger")
        && e.target !== document.querySelector(".menu-container")) {
        console.log(document.querySelectorAll(".link-list-hamburger"))
        console.log(e.target);
        menu.style.display = "none";
        hamburgermenu.src = './images/hamburger-menu-svgrepo-com (1).svg';
    }
});

//محو کردن بکگراند در زمان باز شدن منو
$('.clicker').click(function () {
    $('body').toggleClass('overlay');
});



//با فوکوس بر روی اینپوت بخش منویی باز میشود
// function focusInput(input, result) {
//     var inputvalue = document.getElementById(input);
//     var resultvalue = document.getElementById(result);
//     console.log(inputvalue);
//     console.log(resultvalue);
//     $(inputvalue).focus(function () {
//         resultvalue.show();
//     });
//     $(inputvalue).focusout(function () {
//         resultvalue.hide();
//     });
// }

function focusInput(input, result, container) {
    var inputvalue = document.getElementById(input);
    var resultvalue = document.getElementById(result);
    var containervalue = document.getElementById(container);
    containervalue.classList.add("ticket-inputs-focus");
    inputvalue.style.backgroundColor = '#FFF';
    resultvalue.style.display = 'block';
}
function blurInput(input, result, container) {
    var inputvalue = document.getElementById(input);
    var resultvalue = document.getElementById(result);
    var containervalue = document.getElementById(container);
    containervalue.classList.remove("ticket-inputs-focus");
    inputvalue.style.backgroundColor = '#dcdddd';
    resultvalue.style.display = 'none';
}


function log(msg) {
    const logElem = document.querySelector(".log");

    const time = new Date();
    const timeStr = time.toLocaleTimeString();
    logElem.innerHTML += `${timeStr}: ${msg}<br/>`;
}

log("Logging mouse events inside this container…");

function logEvent(event) {
    const msg = `Event <strong>${event.type}</strong> at <em>${event.clientX}, ${event.clientY}</em>`;
    log(msg);
}

const boxElem = document.querySelector("li");
boxElem.addEventListener("click", logEvent);


